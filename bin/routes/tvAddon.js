/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 12/19/2019
 */

const express = require('express');
const router = express.Router();
const basicAuth = require('../middleware/basicAuth');
const userAuth = require('../middleware/userAuth');

const tvAddOnSubscriptionController = require('../controllers/tvAddOnSubscriptionController');


router.post('/', userAuth, tvAddOnSubscriptionController.subscribeToTVAddOn);

router.get('/', basicAuth.isAuthenticated, tvAddOnSubscriptionController.getAllTVSubscriptions);

router.get('/:userId', userAuth,tvAddOnSubscriptionController.getTVSubscriptionsByUserId);



module.exports = router;