/**
 * Created by nishanif on 15/11/19
 */
const wrapper = require('../../../../helpers/utils/wrapper');
const {ConflictError, NotFoundError} = require('../../../../helpers/error');
const Command = require('./command');
const TVAddOnQuery = require('../queries/query');
const TVAddOnCommand = require('../commands/command');

/**
 * ES Support tvAddon
 */
class TVAddOnSubscription {

    constructor() {
        this.command = new Command();
        this.tvAddOnQuery = new TVAddOnQuery();
        this.tvAddOnCommand = new TVAddOnCommand();
    }

    async createDoc(payload) {
        const now = new Date().toISOString();
        payload.createdAt = now;
        payload.updatedAt = now;
        let result = await this.command.insertDoc(payload);
        if (result.err) {
            return wrapper.error(new ConflictError('Create Failed'));
        }
        return await wrapper.data(result.data);
    }

    async updateSubscriptions(userId, service) {
        const query = {
            match: {
                'userId': userId
            }
        };
        let subscription = await this.tvAddOnQuery.getDocByParam(query);

        if(subscription.err) {
            return wrapper.error(new NotFoundError('Subscription not found'));
        }
        const updatedSubscription = subscription.data;
        updatedSubscription.updatedAt = new Date().toISOString();
        if(updatedSubscription.service.includes(service)) {
            return wrapper.error(new ConflictError(`Already subscribed to ${service}`));
        }

        updatedSubscription.service.push(service);
        const result = await this.tvAddOnCommand.updateDoc(updatedSubscription);
        if(result.err) {
            return  wrapper.error(new ConflictError('Update Subscription failed'));
        }
        return wrapper.data(result.data.result);

    }

}

module.exports = TVAddOnSubscription;

