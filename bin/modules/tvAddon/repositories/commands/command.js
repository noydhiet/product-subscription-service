/**
 * Created by nishanif on 15/11/19
 */
const config = require('../../../../config');
const elastic = require('../../../../helpers/databases/elasticsearch/db');

/**
 * ES functions for TV AddOn Subscription
 */
class TVAddOnSubscription {

    constructor() {
        this.elastic = elastic;
    }

    async insertDoc(payload) {
        const data = {
            index: config.get('/elasticIndexes/tvAddOn'),
            type: '_doc',
            id: payload.userId,
            body: payload
        };

        const result = this.elastic.insertData(config.get('/elasticsearch'), data);
        return result;
    }

    async updateDoc(payload) {
        const userId = payload.userId;
        const subscription = {
            index: config.get('/elasticIndexes/tvAddOn'),
            type: '_doc',
            id: userId,
            body: {
                doc: payload
            }
        };
        const result = this.elastic.updateData(config.get('/elasticsearch'), subscription);
        return result;
    }
}

module.exports = TVAddOnSubscription;
