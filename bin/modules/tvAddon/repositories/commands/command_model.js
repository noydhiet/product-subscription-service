/**
 * Created by nishanif on 15/11/19
 */
const joi = require('@hapi/joi');
/**
 * Object model for TVAddOnSubscription
 */
const TVAddOnSubscription = joi.object({
    indihome_number: joi.string().required(),
    mobile_number: joi.string().required(),
    email: joi.string().required(),
    service: joi.array().required(),
    userId: joi.string().required()
});

module.exports = {
    TVAddOnSubscription
};
