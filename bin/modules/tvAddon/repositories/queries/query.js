/**
 * Created by nishanif on 15/11/19
 */
const config = require('../../../../config');
const elastic = require('../../../../helpers/databases/elasticsearch/db');

/**
 * ES Query for TVAddOn Subscriptions
 */
class Query {

    constructor() {
        this.elastic = elastic;
    }

    /**
     * Get all TVAddOn Subscriptions
     * @returns {Promise<*>}
     */
    async getAllDocs() {
        const query = {
            index: config.get('/elasticIndexes/tvAddOn'),
            type: '_doc',
            body: {
                from: 0,
                size: config.get('/elasticsearch/size'),
                query: {
                    match_all: {}
                }
            }
        };
        const result = this.elastic.findAll(config.get('/elasticsearch'), query);
        return result;
    }

    /**
     * Get a TVAddOn Subscriptions by param filter
     * @param {Object} param -filter by param
     * @returns {Promise<*>}
     */
    async getDocByParam(param) {
        const query = {
            index: config.get('/elasticIndexes/tvAddOn'),
            type: '_doc',
            body: {
                query: param
            }
        };
        const result = this.elastic.findOne(config.get('/elasticsearch'), query);
        return result;
    }

    /**
     * Get all TVAddOn Subscriptions by param filter
     * @param {Object} param -filter by param
     * @returns {Promise<*>}
     */
    async getDocsByParam(param) {
        const query = {
            index: config.get('/elasticIndexes/tvAddOn'),
            type: '_doc',
            body: {
                from: 0,
                size: config.get('/elasticsearch/size'),
                query: param
            }
        };
        const result = this.elastic.findAll(config.get('/elasticsearch'), query);
        return result;
    }

}

module.exports = Query;

