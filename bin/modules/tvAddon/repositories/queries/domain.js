/**
 * Created by nishanif on 15/11/19
 */
const Query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const {NotFoundError} = require('../../../../helpers/error');

/**
 * ES Query request for TVAddOn
 */
class TVAddOnSubscription {

    constructor() {
        this.query = new Query();
    }

    async getAllTVSubscriptions() {
        const result = await this.query.getAllDocs();
        if (result.err) {
            return wrapper.error(new NotFoundError('TVAddOnSubscription data not found'));
        }

        return wrapper.data(result.data);
    }

    async getTVSubscriptionsByUserId(userID) {
        const query = {
            bool: {
                must: [
                    {
                        match: {
                            'userId': userID
                        }
                    }
                ],
            }
        };
        const result = await this.query.getDocByParam(query);
        if (result.err) {
            return wrapper.error(new NotFoundError('TVAddOnSubscription data not found'));
        }
        const { userId, indihome_number, mobile_number, email, service } = result.data;
        const data = {
            userId,
            indihome_number,
            mobile_number,
            email,
            service
        };

        return wrapper.data(data);
    }
}

module.exports = TVAddOnSubscription;
