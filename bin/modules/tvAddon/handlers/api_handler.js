/**
 * Created by nishanif on 15/11/19
 */
const TVAddOnCommand = require('../repositories/commands/domain');
const TVAddOnQuery = require('../repositories/queries/domain');
const validator = require('../../../helpers/utils/validator');
const commandModel = require('../repositories/commands/command_model');
const wrapper = require('../../../helpers/utils/wrapper');

/**
 * Create index on elastic search
 * @param {Object} payload - request payload
 * @returns {Promise<*>}
 */
const createSubscription = async (payload) => {
    const validatePayload = validator.isValidPayload(payload, commandModel.TVAddOnSubscription);
    const tvAddOnCommand = new TVAddOnCommand();
    const postRequest = async (result) => {
        if (result.err) {
            return result;
        }
        return tvAddOnCommand.createDoc(payload);
    };
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        return result.err? result.err : result.data;
    };

    return sendResponse(await postRequest(validatePayload));
};

/**
 * Update subscriptions on elastic search
 *
 * @param {String} userId - user Id
 * @param {String} service - service
 * @returns {Promise<*|{err, data}>}
 */
const updateSubscriptions = async (userId, service) => {
    const tvAddOnCommand = new TVAddOnCommand();
    const putRequest = async () => tvAddOnCommand.updateSubscriptions(userId, service);

    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        return result.err? result.err : wrapper.data(result.data);
    };

    return  sendResponse(await putRequest());
};

/**
 * Get all TV Add on subscriptions
 * @returns {Promise<void>}
 */
const getAllTVSubscriptions = async () => {
    const tvAddOnQuery = new TVAddOnQuery();
    const getRequest = async () => tvAddOnQuery.getAllTVSubscriptions();
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        return result.err? result.err : wrapper.data(result.data);
    };
    return sendResponse(await getRequest());
};

/**
 * Get all TV Add on subscriptions by userId
 * @param userId
 * @returns {Promise<*|{err, data}>}
 */
const getTVSubscriptionsByUserId = async (userId) => {
    const tvAddOnQuery = new TVAddOnQuery();
    const getRequest = async () => tvAddOnQuery.getTVSubscriptionsByUserId(userId);

    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        return result.err? result.err : wrapper.data(result.data);
    };

    return sendResponse(await getRequest());
};

module.exports = {
    createSubscription,
    getAllTVSubscriptions,
    getTVSubscriptionsByUserId,
    updateSubscriptions
};
