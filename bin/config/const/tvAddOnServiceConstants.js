/*
* Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Created by Nishani Fernando on 18/11/19
*/

module.exports = {
    ADDON_TYPES: [
        'hooq',
        'iflix',
        'catchplay',
        'edukids' //@todo replace with actual value
    ],
    ADDON_ERRORS: {
        ALREADY_SUBSCRIBED: 'IndiHome Number Already Have Active Subscription',
        ONLY_ALLOWED_TO_TRIPLE_PLAY_CUSTOMERS:
            'Error, Maaf, layanan iflix hanya bisa dinikmati oleh pelanggan IndiHome triple play (Telepon,Internet, UseeTV)'
    }
};
