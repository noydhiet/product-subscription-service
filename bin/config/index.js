/*
* Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Created by Nishani Fernando on 15/11/19
*/
require('dotenv').config();
const confidence = require('confidence');

const config = {
    port: process.env.PORT,
    authorization: {
        host: process.env.AUTHORIZATION_API_HOST,
        endpoint: process.env.AUTHORIZATION_API_ENDPOINT,
        token: process.env.AUTHORIZATION_TOKEN
    },
    authentication: {
        token: process.env.SHARED_SECRET
    },
    basicAuthApi: [
        {
            username: process.env.BASIC_AUTH_USERNAME,
            password: process.env.BASIC_AUTH_PASSWORD
        }
    ],
    identityBaseUrl: process.env.IDENTITY_BASE_URL,
    authenticationPort: process.env.AUTHENTICATION_PORT,
    indihomeBaseUrl: process.env.INDIHOME_BASE_URL,
    indihomeBasicAuth: process.env.INDIHOME_BASIC_AUTH,
    jwtAuthKey: process.env.JWT_AUTH_KEY,
    dsnSentryUrl: process.env.DSN_SENTRY_URL,
    elasticsearch: {
        connectionClass: process.env.ELASTICSEARCH_CONNECTION_CLASS,
        apiVersion: process.env.ELASTICSEARCH_API_VERSION,
        host: [
            process.env.ELASTICSEARCH_HOST
        ],
        maxRetries: process.env.ELASTICSEARCH_MAX_RETRIES,
        requestTimeout: process.env.ELASTICSEARCH_REQUEST_TIMEOUT,
        size: process.env.ELASTICSEARCH_MAX_SIZE
    },
    elasticIndexes: {
        tvAddOn: process.env.ELASTICSEARCH_TV_SUBSCRIPTION_INDEX
    },
    tvAddOnSubscriptionService: {
        endpoint: process.env.TV_ADD_ON_ACTIVATION_ENDPOINT
    }
};

const store = new confidence.Store(config);

exports.get = (key) => store.get(key);
