/*
* Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Created by Nishani Fernando on 18/11/19
*/
const rp = require('request-promise');
const _ = require('lodash');
const log4js = require('log4js');
const logger = log4js.getLogger('ProductSubscriptionService');
logger.level = 'info';
const config = require('../config');
const authenticationService = require('./clientAuthorizationService');
const {BadRequestError, ConflictError, ForbiddenError} = require('../helpers/error');

const tvAddOnSubscriptionServiceEndpoint = config.get('/tvAddOnSubscriptionService').endpoint;
const tvAddOnHandler = require('../modules/tvAddon/handlers/api_handler');
const tvAddOnServiceConstants = require('../config/const/tvAddOnServiceConstants');

/**
 * Subscribe to TV Add-on service
 *
 * @param {String} subscriptionPoint - Subscription Point data
 * @param {String} selectedIndiHomeNo - selected IndiHomeNo
 * @param {Object} userData - User details
 * @returns {Promise<{issueId: (*|string), status: string}>}
 */
module.exports.subscribeToTVAddOn = async (subscriptionPoint, selectedIndiHomeNo, userData) => {
    const token = await authenticationService.getJWTFForUser();
    const authorizationHeader = 'Bearer ' + token;
    if (_.isEmpty(subscriptionPoint) || _.isEmpty(selectedIndiHomeNo)) {
        throw new BadRequestError('service or selected IndiHome Number cannot be empty');
    }
    if (_.isEmpty(userData)) {
        throw new ForbiddenError('Unauthorized request');
    }

    // validate subscription service
    if (!tvAddOnServiceConstants.ADDON_TYPES.includes(subscriptionPoint)) {
        throw new BadRequestError('Invalid subscription type');
    }

    logger.info('User data: ' + JSON.stringify(userData));
    logger.info('Product data: ' + JSON.stringify(subscriptionPoint));

    const reqBody = {
        indihome_number: selectedIndiHomeNo,
        mobile_number: userData.mobile,
        email: userData.email,
        service: subscriptionPoint
    };

    const options = {
        method: 'POST',
        uri: tvAddOnSubscriptionServiceEndpoint,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorizationHeader
        },
        json: reqBody,
        strictSSL: false
    };

    logger.info(`TV ADDON SUBSCRIPTION SVC: Date: ${new Date()}| sending request to create subscription. Req: ${JSON.stringify(options)}`);
    const response = await rp.post(options);
    logger.info(`TV ADDON SUBSCRIPTION  SVC: Date: ${new Date()}| received response data from subscription. Response: ${JSON.stringify(response)}`);

    if (response.statusCode === '0') {
        let subscription = await tvAddOnHandler.getTVSubscriptionsByUserId(userData.userId);
        if (!subscription.err) {
            if(_.isEmpty(subscription.data)) {
                // add new subscription
                const data = Object.assign(reqBody, {});
                data.userId = userData.userId;
                //execute elastic search indexing by userId
                data.service = [reqBody.service];
                let created = await tvAddOnHandler.createSubscription(data);
                if (created.err) {
                    throw new ForbiddenError('Failed to create entry.');
                }
            } else {
                // update already existing subscriptions
                const updatedSubscription = subscription.data;
                if(updatedSubscription.service.includes(reqBody.service)) {
                    throw new ConflictError(`Already subscribed to ${reqBody.service}`);
                }

                updatedSubscription.service.push(reqBody.service);
                let created = await tvAddOnHandler.createSubscription(updatedSubscription);
                if (created.err) {
                    throw new ForbiddenError('Failed to create entry.');
                }
            }
        }

        return {
            status: 'success',
            statusCode: 201,
            data: response.data
        };
    } else if (response.statusCode === '-2') {
        if (response.returnMessage === tvAddOnServiceConstants.ADDON_ERRORS.ALREADY_SUBSCRIBED) {
            throw new ConflictError('This IndiHome Number already has an active subscription');
        } else if (response.returnMessage === tvAddOnServiceConstants.ADDON_ERRORS.ONLY_ALLOWED_TO_TRIPLE_PLAY_CUSTOMERS) {
            throw new ForbiddenError('iflix service can only be enjoyed by IndiHome triple play customers (Telephone, Internet, UseeTV)');
        }
    }

    throw new BadRequestError();
};

/**
 * Get all TV Subscriptions
 * @returns {Promise<void>}
 */
module.exports.getAllTVSubscriptions = async () => {
    const received = await tvAddOnHandler.getAllTVSubscriptions();
    if (received.err) {
        throw new ForbiddenError();
    }
    return received;
};

/**
 * Get all TV Subscriptions by User Id
 * @param {String} userId - User Id
 * @returns {Promise<void>}
 */
module.exports.getTVSubscriptionsByUserId = async (userId) => {
    if (_.isEmpty(userId)) {
        throw new BadRequestError();
    }
    const received = await tvAddOnHandler.getTVSubscriptionsByUserId(userId);
    if (received.err) {
        throw new ForbiddenError();
    }
    return received;
};