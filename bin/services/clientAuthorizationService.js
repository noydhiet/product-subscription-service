/*
* Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Created by Nishani Fernando on 15/11/19
*/

const util = require('util');
const request = require('request-promise');
const log4js = require('log4js');
const logger = log4js.getLogger('AuthenticationService');
const config = require('../config');


module.exports.getJWTFForUser = () => new Promise((resolve, reject) => {
    try {
        let token = getJWT();
        resolve(token);
    } catch (error) {
        logger.error(error);
        reject();
    }
});

/**
 * Get access token form the cline API using Basic Authentication
 *
 * user String username
 * secret String password
 **/
async function getJWT() {

    //todo:  can store the token in this method for configurable time and reuse
    //todo: get the confirmation on what really is the token invalidation time for the API gateway
    let options = {
        method: 'GET',
        url: config.get('/authorization').host + config.get('/authorization').endpoint,
        headers:
            {
                'Authorization': 'Basic ' + config.get('/authorization').token,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        json: true,
        strictSSL: false
    };

    let resp = await request(options);
    return !util.isNullOrUndefined(resp) && !util.isNullOrUndefined(resp.data) ? resp.data['token'] : null;

}
