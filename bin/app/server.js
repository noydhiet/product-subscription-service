/*
* Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Created by Nishani Fernando on 18/11/19
*/

const express = require('express');
const cors = require('cors');
const swaggerUi = require('swagger-ui-express');
const wrapper = require('../helpers/utils/wrapper');
const bodyParser = require('body-parser');

const fs = require('fs');
const path = require('path');
const spec = fs.readFileSync(path.join(__dirname, '../swagger/swagger.yaml'), 'utf8');

const jsyaml = require('js-yaml');
const swaggerDocument = jsyaml.safeLoad(spec);

const userAuth = require('../middleware/userAuth');
const basicAuth = require('../middleware/basicAuth');

//routes
const tvAddonRoutes = require('../routes/tvAddon');

function AppServer() {
    this.server = express();

    this.server.use(bodyParser.json());

    this.server.use(cors());
    this.server.use(basicAuth.init());

    this.server.get('/', (req, res) => {
        wrapper.response(res, 'success', wrapper.data('Product Subscription Services'), 'This services is running properly.');
    });

    this.server.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

    // OTT subscription: TV Add On
    this.server.use('/product-subscription/tv-addon', tvAddonRoutes);
}

module.exports = AppServer;
