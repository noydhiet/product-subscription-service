/*
* Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Created by Nishani Fernando on 18/11/19
*/

const _ = require('lodash');

const tvAddOnSubscriptionService = require('../services/tvAddOnSubscriptionService');
const responseHandler = require('../helpers/utils/wrapper');
const { RequestTimeoutError } = require('../helpers/error');

/**
 * Subscribe to TV Add On
 */
module.exports.subscribeToTVAddOn = (req, res) => {
    //@todo add validator for req data
    const userData = req.userData;
    const serviceName = req.query.service;
    const selectedIndiHomeNo = req.query.selectedIndiHomeNum;

    tvAddOnSubscriptionService.subscribeToTVAddOn(serviceName, selectedIndiHomeNo,  userData)
        .then(resp => {
            responseHandler.response(res, 'success', responseHandler.data(resp), 'Subscribed to TV ADD-ON service successfully', 201);
        })
        .catch(err => {
            if(err.statusCode === 408) {
                responseHandler.response(res, 'fail', responseHandler.error(new RequestTimeoutError('Error in subscription service')));
            } else {
                responseHandler.response(res, 'fail', responseHandler.error(err));
            }
        });
};

/**
 * Get All TV AddOn subscriptions
 */
module.exports.getAllTVSubscriptions = (req, res) => {
    tvAddOnSubscriptionService.getAllTVSubscriptions()
        .then(resp => {
            //@todo: move response type, status codes to constants
            if(_.isEmpty(resp)) {
                responseHandler.response(res, 'success', responseHandler.data(resp),
                    'Currently there are no TV AddOn subscriptions', 201);
            } else {
                responseHandler.response(res, 'success', responseHandler.data(resp),
                    'Retrieved all TV AddOn subscriptions successfully', 201);
            }

        })
        .catch(err => {
            responseHandler.response(res, 'fail', responseHandler.error(err));
        });
};

/**
 * Get TV AddOn subscriptions by userId
 */
module.exports.getTVSubscriptionsByUserId = (req, res) => {
    // @todo: get userId from auth data
    let userId = req.params.userId;
    tvAddOnSubscriptionService.getTVSubscriptionsByUserId(userId)
        .then(resp => {
            //@todo: move response type, status codes to constants
            if(_.isEmpty(resp)) {
                responseHandler.response(res, 'success', responseHandler.data(resp),
                    'Currently there are no TV AddOn subscriptions for the user', 201);
            } else {
                responseHandler.response(res, 'success', responseHandler.data(resp),
                    'Retrieved all TV AddOn subscriptions by user successfully', 201);
            }
        })
        .catch(err => {
            responseHandler.response(res, 'fail', responseHandler.error(err));
        });
};
