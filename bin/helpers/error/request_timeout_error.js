/*
* Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Created by Nishani Fernando on 20/11/19
*/
const CommonError = require('./common_error');

class RequestTimeoutError extends CommonError {
    constructor(message) {
        super(message || 'Request timeout');
    }
}

module.exports = RequestTimeoutError;
