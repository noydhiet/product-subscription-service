const {BadRequestError} = require('../error');
const wrapper = require('./wrapper');

const isValidPayload = (payload, constraint) => {
    const {value, error} = constraint.validate(payload);
    if (error) {
        const message = error.details.shift().message.replace(/"/g, '');
        return wrapper.error(new BadRequestError(message));
    }
    return wrapper.data(value);

};

module.exports = {
    isValidPayload
};
