const crypto = require('crypto');

const generateRandomString = (length) => {
    const rand = crypto.randomBytes(20).toString('hex');
    return rand;
};

module.exports = {
    generateRandomString
};
