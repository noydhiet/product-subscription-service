/*
* Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Created by Nishani Fernando on 15/11/19
*/

const passport = require('passport');
const {BasicStrategy} = require('passport-http');
const config = require('../config');

class User {
    constructor(username, password) {
        this.username = username;
        this.password = password;
    }

    isValidPassword(password) {
        return this.password === password;
    }

}

function findByUsername(username, cb) {
    const userDatas = config.get('/basicAuthApi');
    let userData;

    userData = userDatas.map((value) => {
        if (value.username === username) {
            return value;
        }
        return '';
    });
    const user = new User(userData[0].username, userData[0].password);
    cb(user);
}

passport.use(new BasicStrategy((username, password, cb) => {
    findByUsername(username, (user) => {
        if (!user) {
            return cb(null, false);
        }
        if (!user.isValidPassword(password)) {
            return cb(null, false);
        }
        return cb(null, user);

    });
}));

module.exports.isAuthenticated = passport.authenticate('basic', {session: false});

module.exports.init = () => passport.initialize();
