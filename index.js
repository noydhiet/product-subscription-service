/**
 * Created by nishanif on 15/11/19
 */

// Logger config
const log4js = require('log4js');
const logger = log4js.getLogger('App');
logger.level = 'info';

const AppServer = require('./bin/app/server');
const config = require('./bin/config');

const appServer = new AppServer();
const port = process.env.port || config.get('/port') || 8081;

appServer.server.listen(port, () => {

    logger.info('Your server is listening on port %d (http://localhost:%d)', port, port);
    logger.info('Swagger-ui is available on http://localhost:%d/docs', port);
});
