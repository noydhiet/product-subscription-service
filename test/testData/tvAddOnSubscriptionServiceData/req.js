/*
* Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Created by Nishani Fernando on 18/11/19
*/
/**
 * Test data for TV AddOn subscription tests
 */
module.exports = {
    tvAddOn: {
        hooq:'hooq',
        catchplay:'catchplay',
        iflix:'iflix'
    },
    tvAddOnInvalid: 'test',
    indiHomeNumber: {
        new_subscription: '122874254820',
        existing_subscription: '121302218508',
        triple_play_customer: '121302218508'
    },
    userData: {
        "email": "nlfernando11@gmail.com",
        "userId": "5dcbc146024ae600194f0c85",
        "userRole": ["generalUser"],
        "mobile": "123213124",
        "iat": 1574050744,
        "exp": 1574054344
    }
};