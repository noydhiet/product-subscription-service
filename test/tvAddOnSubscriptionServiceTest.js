/*
* Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Created by Nishani Fernando on 18/11/19
*/

'use strict';

const tvAddOnSubscriptionService = require("../bin/services/tvAddOnSubscriptionService");
let chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
const expect = chai.expect;
const testData = require("./testData/tvAddOnSubscriptionServiceData/req");

describe('TV ADD ON Point Subscription', function () {
    describe('Subscribe to TV add-on HOOQ service: Valid Params', function () {
        it('Should Subscribe to HOOQ Successfully', function () {
            this.timeout(100000);
            let response = tvAddOnSubscriptionService.subscribeToTVAddOn(testData.tvAddOn.hooq, testData.userData);

            return response.then(data => {
                expect(data).not.to.be.empty;
                expect(data.status).to.be.equal('success');
            });
        });

        it('Should Fail to HOOQ as already existing', function () {
            this.timeout(100000);
            let response = tvAddOnSubscriptionService.subscribeToTVAddOn(testData.tvAddOn.hooq, testData.indiHomeNumber.new_subscription, testData.userData);

            return response.catch(status => {
                expect(status).to.be.empty;
            });
        });

        it('Should Fail to Subscribe to HOOQ', function () {
            this.timeout(100000);
            let response = tvAddOnSubscriptionService.subscribeToTVAddOn(null, testData.indiHomeNumber.new_subscription, testData.userData);

            return response.catch(status => {
                expect(status).to.be.empty;
            });
        });
    });

    describe('Subscribe to TV add-on Catch Play service', function () {
        it('Should Subscribe to CatchPlay Successfully', function () {
            this.timeout(100000);
            let response = tvAddOnSubscriptionService.subscribeToTVAddOn(testData.tvAddOn.catchplay, testData.indiHomeNumber.new_subscription,  testData.userData);

            return response.then(data => {
                expect(data).not.to.be.empty;
                expect(data.status).to.be.equal('success');
            });
        });

        it('Should Fail to CatchPlay as already existing', function () {
            this.timeout(100000);
            let response = tvAddOnSubscriptionService.subscribeToTVAddOn(testData.tvAddOn.catchplay, testData.indiHomeNumber.new_subscription, testData.userData);

            return response.catch(status => {
                expect(status).to.be.empty;
            });
        });

        it('Should Fail to Subscribe to CatchPlay', function () {
            this.timeout(100000);
            let response = tvAddOnSubscriptionService.subscribeToTVAddOn(null, testData.indiHomeNumber.new_subscription, testData.userData);

            return response.catch(status => {
                expect(status).to.be.empty;
            });
        });
    });

    describe('Subscribe to TV add-on iFlix service', function () {
        it('Should Subscribe to iflix Successfully', function () {
            this.timeout(100000);
            let response = tvAddOnSubscriptionService.subscribeToTVAddOn(testData.tvAddOn.iflix, testData.indiHomeNumber.new_subscription, testData.userData);

            return response.then(data => {
                expect(data).not.to.be.empty;
                expect(data.status).to.be.equal('success');
            });
        });

        it('Should Fail to iflix as already existing', function () {
            this.timeout(100000);
            let response = tvAddOnSubscriptionService.subscribeToTVAddOn(testData.tvAddOn.iflix, testData.indiHomeNumber.new_subscription, testData.userData);

            return response.catch(status => {
                expect(status).to.be.empty;
            });
        });

        it('Should Fail to Subscribe to iflix', function () {
            this.timeout(100000);
            let response = tvAddOnSubscriptionService.subscribeToTVAddOn(null, testData.indiHomeNumber.new_subscription, testData.userData);

            return response.catch(status => {
                expect(status).to.be.empty;
            });
        });
    });

    describe('Subscribe to TV add-on service: Invalid Params', function () {
        it('Should Fail to Subscribe to any TV Add On', function () {
            this.timeout(100000);
            let response = tvAddOnSubscriptionService.subscribeToTVAddOn(testData.tvAddOnInvalid, testData.indiHomeNumber.new_subscription, testData.userData);

            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
    });

    describe("Get All TV add-on subscriptions", function () {
        it("Should fetch all TV add-on subscriptions successfully", function () {
            this.timeout(100000);
            let response = tvAddOnSubscriptionService.getAllTVSubscriptions();
            return response.then(data => {
                expect(data.data).not.to.be.empty;
            });
        });
    });

    describe("Get All TV add-on subscriptions by user", function () {
        it("Should fetch all TV add-on subscriptions by user successfully", function () {
            this.timeout(100000);
            let response = tvAddOnSubscriptionService.getTVSubscriptionsByUserId(testData.userData.userId);
            return response.then(data => {
                expect(data.data).not.to.be.empty;
            });
        });
    });
});